<?php

namespace Tests;

use App\Models\User;
use Illuminate\Foundation\Testing\TestCase as BaseTestCase;
use Laravel\Passport\Passport;

abstract class TestCase extends BaseTestCase
{
    use CreatesApplication;

    public function createUser()
    {
        return User::factory()->create();

    }

    public function adminIsLogin()
    {
        $user = User::find(5);
        Passport::actingAs($user);
    }

    public function userIsLogin()
    {
        $user = $this->createUser();
        Passport::actingAs($user);
    }
}
