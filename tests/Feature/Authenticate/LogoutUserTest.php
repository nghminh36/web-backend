<?php

namespace Tests\Feature;

use App\Models\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Http\Response;
use Illuminate\Testing\Fluent\AssertableJson;
use Laravel\Passport\Passport;
use Tests\TestCase;

class LogoutUserTest extends TestCase
{
    /** @test */
    public function user_loggedin_can_be_logout()
    {
        $this->userIsLogin();
        $response = $this->postJson('/api/logout');
        $response->assertStatus(Response::HTTP_OK);
        $response
            ->assertJson(fn (AssertableJson $json) =>
                $json->where('status', Response::HTTP_OK)
                    ->where('message', 'You are logged out!')
                );
    }

    /** @test */
    public function unauthenticated_user_can_not_logout()
    {
        $response = $this->postJson('/api/logout');
        $response->assertStatus(Response::HTTP_UNAUTHORIZED);
    }
}
