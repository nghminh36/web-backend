<?php

namespace Tests\Feature;

use Illuminate\Http\Response;
use Illuminate\Testing\Fluent\AssertableJson;
use Laravel\Passport\Passport;
use Tests\TestCase;

class DeleteUserTest extends TestCase
{
    /** @test */
    public function authenticated_and_has_permission_user_can_delete_user_account()
    {
        $this->withoutExceptionHandling();
        $user = $this->createUser();
        $this->adminIsLogin();
        $response = $this->json('DELETE','/api/users/'.$user->id);
        $response->assertStatus(Response::HTTP_OK);
        $response
            ->assertJson(fn (AssertableJson $json) =>
            $json->where('status', Response::HTTP_OK)
                ->where('message', 'Delete successfully')
                ->etc()
            );
    }

    /** @test */
    public function unauthenticated_user_can_not_delete_user_account()
    {
        $user = $this->createUser();
        $response = $this->json('DELETE','/api/users/'.$user->id);
        $response->assertStatus(Response::HTTP_UNAUTHORIZED);
    }
}
