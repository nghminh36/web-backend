<?php

namespace Tests\Feature;

use App\Models\User;
use Illuminate\Testing\Fluent\AssertableJson;
use Illuminate\Http\Response;
use Laravel\Passport\Passport;
use Tests\TestCase;

class GetUsersTest extends TestCase
{
    /** @test */
    public function authenticated_and_has_permission_user_can_get_users_list()
    {
        $this->adminIsLogin();
        $response = $this->getJson('/api/users');
        $response->assertStatus(Response::HTTP_OK);
        $response
            ->assertJson(fn (AssertableJson $json) =>
            $json->where('status', Response::HTTP_OK)
                ->where('data.id', 1)
                ->where('data.name', 'admin')
                ->missing('data.password')
                 ->etc()
        );
    }

    /** @test */
    public function unauthenticated_user_can_not_get_users_list()
    {
        $response = $this->getJson('/api/users');
        $response->assertStatus(Response::HTTP_UNAUTHORIZED);
    }

}
