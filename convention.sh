phpcs --standard=PSR2 app/Models
phpcs --standard=PSR2 app/Services
phpcs --standard=PSR2 app/Repositories
phpcs --standard=PSR2 app/Http/Controllers
