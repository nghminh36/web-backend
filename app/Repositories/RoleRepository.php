<?php

namespace App\Repositories;

use App\Models\Role;

class RoleRepository extends BaseRepository
{
    protected function model()
    {
        return Role::class;
    }

    public function store(array $data)
    {
        return $this->model->create($data);
    }

    public function findById($id)
    {
        return $this->model->findOrFail($id);
    }

    public function searchWithPaginate(array $data)
    {
        return $this->model->searchByRole($data['name'])->latest('id')->paginate($data['quantity'] ?? 5);
    }
}
