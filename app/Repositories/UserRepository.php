<?php

namespace App\Repositories;

use App\Models\User;

class UserRepository extends BaseRepository
{

    protected function model()
    {
        return User::class;
    }

    public function create(array $data)
    {
        $user =  $this->model->create($data);
        $user->assignRole($data['role']);
        return $user;
    }

    public function update($request, $id)
    {
        $dataUpdate = $request->all();
        $user = $this->model->findOrFail($id);
        $user->update($dataUpdate);
        if ($request->has('role')) {
            $user->syncRoles($request->role);
        }
        return $user;
    }

    public function show($id)
    {
        return $this->model->findOrFail($id);
    }

    public function findByEmail(string $email)
    {
        return $this->model->where('email', $email)->first();
    }

    public function searchWithPaginate(array $data)
    {
        return $this->model->searchByName($data['name'])->latest('id')->paginate($data['quantity'] ?? 5);
    }

    public function destroy($id)
    {
        $user = $this->model->findOrFail($id);
        $user->delete();
        return $user;
    }
}
