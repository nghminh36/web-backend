<?php

namespace App\Http\Controllers\Login;

use App\Http\Controllers\Controller;
use App\Http\Requests\LoginUserRequest;
use App\Http\Resources\UserResource;
use App\Services\UserService;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Auth;

class LoginController extends Controller
{
    protected $userService;

    public function __construct(UserService $userService)
    {
        $this->userService = $userService;
    }

    public function login(LoginUserRequest $request)
    {
        if (Auth::attempt($request->only('email', 'password'))) {
            $user = $this->userService->findByEmail($request);
            $token = $user->createToken('Personal Access Token');
            return $this->sendResponse([
                'user' => new UserResource($user),
                'access_token' => $token->accessToken,
            ], 'User login successfully!!!');
        } else {
            return $this->failResponse('Unauthorized');
        }
    }

    public function logout(Request $request)
    {
        $request->user()->token()->revoke();
        return response()->json([
            'status' => Response::HTTP_OK,
            'message' => 'You are logged out!',
            ], Response::HTTP_OK);
    }
}
