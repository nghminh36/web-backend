<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Requests\CreateUserRequest;
use App\Http\Requests\UpdateUserRequest;
use App\Http\Resources\UserCollection;
use App\Http\Resources\UserResource;
use App\Services\UserService;
use Illuminate\Http\Request;

class UserController extends Controller
{
    protected $userService;

    public function __construct(UserService $userService)
    {
        $this->userService = $userService;
    }

    public function index(Request $request)
    {
        $usersWithPaginate = $this->userService->getWithPaginate($request);
        $users = new UserCollection($usersWithPaginate);
        return $this->sendResponse($users, 'Show users list successfully!!!');
    }

    public function store(CreateUserRequest $request)
    {
        $user = $this->userService->create($request);
        return $this->sendResponse(new UserResource($user), 'Create user successfully!!!');
    }

    public function show(int $id)
    {
        $user = $this->userService->show($id);
        return $this->sendResponse(new UserResource($user), 'Show a user successfully!!!');
    }

    public function update(UpdateUserRequest $request, int $id)
    {
        $user = $this->userService->update($request, $id);
        return $this->sendResponse(new UserResource($user), 'Update successfully');
    }

    public function destroy(int $id)
    {
        $user = $this->userService->destroy($id);
        return $this->sendResponse(new UserResource($user), 'Delete successfully');
    }
}
