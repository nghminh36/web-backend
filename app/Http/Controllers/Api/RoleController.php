<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Requests\RoleRequest;
use App\Http\Resources\PermissionCollection;
use App\Http\Resources\RoleResource;
use App\Services\PermissionService;
use App\Services\RoleService;
use Illuminate\Http\Request;

class RoleController extends Controller
{

    protected $roleService;
    protected $permissionService;

    public function __construct(RoleService $roleService, PermissionService $permissionService)
    {
        $this->roleService = $roleService;
        $this->permissionService = $permissionService;
    }

    public function index(Request $request)
    {
        $rolesWithPaginate = $this->roleService->getWithPaginate($request);
        $rolesResources = RoleResource::collection($rolesWithPaginate)->response()->getData(true);
        $permissions = new PermissionCollection($this->permissionService->getAllPermission());
        return $this->sendResponse(['roles' => $rolesResources, 'permissions' => $permissions], 'Show role list successfully!!!');
    }

    public function store(RoleRequest $request)
    {
        $role = $this->roleService->store($request);

        return $this->sendResponse(new RoleResource($role), 'Role created successfully');
    }

    public function show($id)
    {
        $role = $this->roleService->show($id);
        return $this->sendResponse(new RoleResource($role), 'Show successfully');
    }

    public function update(RoleRequest $request, $id)
    {
        $role = $this->roleService->update($request, $id);
        return $this->sendResponse(new RoleResource($role), 'Role updated successfully');
    }

    public function destroy($id)
    {
        $role = $this->roleService->destroy($id);
        return $this->sendResponse(new RoleResource($role), 'Role deleted successfully!!!');
    }
}
