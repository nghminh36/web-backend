<?php

namespace App\Http\Requests;

class CreateUserRequest extends BaseRequest
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'email' => 'bail|required|email|unique:users',
            'password' => 'bail|required|string|min:8',
            'phone' => 'bail|digits:10',
        ];
    }
}
