<?php

namespace App\Http\Requests;

class RoleRequest extends BaseRequest
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'name' => 'required',
            'permission' => 'required'
        ];
    }
}
