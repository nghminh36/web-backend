<?php

namespace App\Services;

use App\Repositories\RoleRepository;

class RoleService
{
    protected $roleRepository;

    public function __construct(RoleRepository $roleRepository)
    {
        $this->roleRepository = $roleRepository;
    }

    public function getWithPaginate($request)
    {
        $data = $request->all();
        $data['name'] = $request->name ?? null;
        $data['quantity'] = $request->quantity ?? 5;
        return $this->roleRepository->searchWithPaginate($data);
    }

    public function show($id)
    {
        return $this->roleRepository->findById($id);
    }

    public function store($request)
    {
        $dataCreate = $request->except('permission');
        $dataPermissions = $this->formatRequest($request);
        $role = $this->roleRepository->updateOrCreate($dataCreate);
        $role->syncPermissions($dataPermissions);
        return $role;
    }

    public function update($request, $id)
    {
        $role = $this->roleRepository->findById($id);
        $roleUpdate = $request->all();
        $dataPermissions = $this->formatRequest($request);
        $role->update($roleUpdate);
        $role->syncPermissions($dataPermissions);
        return $role;
    }

    public function destroy($id)
    {
        $role = $this->roleRepository->findById($id);
        $role->delete();
        return $role;
    }

    public function formatRequest($request): array
    {
        $permissions = $request->permission;
        if (is_array($permissions)) {
            $dataPermissions = $permissions;
        } else {
            $dataPermissions = [$permissions];
        }
        return $dataPermissions;
    }
}
