<?php

namespace App\Services;

use App\Repositories\UserRepository;
use Illuminate\Http\Request;

class UserService
{
    protected $userRepository;

    public function __construct(UserRepository $userRepository)
    {
        $this->userRepository = $userRepository;
    }

    public function create(Request $request)
    {
        $dataCreate = $request->all();
        $dataCreate['password'] = $request->password;
        return $this->userRepository->create($dataCreate);
    }

    public function findByEmail(Request $request)
    {
        return $this->userRepository->findByEmail($request->email);
    }

    public function getWithPaginate($request)
    {
        $data = $request->all();
        $data['name'] = $request->name ?? null;
        $data['quantity'] = $request->quantity ?? 5;
        return $this->userRepository->searchWithPaginate($data);
    }

    public function show($id)
    {
        return $this->userRepository->show($id);
    }

    public function update($request, $id)
    {
        return $this->userRepository->update($request, $id);
    }

    public function destroy($id)
    {
        return $this->userRepository->destroy($id);
    }
}
