<?php

return [
    'permission' => [
        'role-list',
        'role-create',
        'role-edit',
        'role-delete',
    ],
//
//    'roles' => [
//        'user' => [
//            'index-user',
//        ],
//        'admin'=>[],
//    ],
];
